;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 display server.

(use-modules (gnu)
	     (guix store) ;for %default-substitue-urls
	     (gnu services base)) ;for %default-authorized-guix-keys
(use-service-modules networking ssh)
(use-package-modules admin)

(operating-system
  (host-name "media01")
  (timezone "EST5EDT")
  (locale "en_US.UTF-8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root" is
  ;; the label of the target root file system.
  (bootloader (grub-configuration (device "/dev/sda")))
  (file-systems (cons (file-system
                        (device "root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "james")
                (comment "James Richardson")
                (group "users")

                ;; Adding the account to the "wheel" group
                ;; makes it a sudoer.  Adding it to "audio"
                ;; and "video" allows the user to play sound
                ;; and access the webcam.
                (supplementary-groups '("wheel"
                                        "audio" "video"))
                (home-directory "/home/james"))
               %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons tcpdump %base-packages))

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services (cons*
	     (dhcp-client-service)
	     (lsh-service #:port-number 2222)
	     ;; add thor server to the list of substite-urls.
	     (modify-services %base-services
			      (guix-service-type config =>
						 (guix-configuration
						  (inherit config)
						  (substitute-urls
						   (cons* "https://thor.lab01.jamestechnotes.com";
							  %default-substitute-urls))
						  (authorized-keys
						   (cons* (plain-file "thor.lab01.jamestechnotes.com"
								      (string-append "(public-key 
									(ecc 
									 (curve Ed25519)
									 (q #3BDE6B5500DD300D267F8187BA8B30BDCF326BD882491F8A31DC7A2B88D9475B#)))"))
							  %default-authorized-guix-keys))))))))
