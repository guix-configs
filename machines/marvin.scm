;; This is an operating system configuration template
;; for a "desktop" setup without full-blown desktop
;; environments.

(use-modules (gnu) 
	     (gnu system nss)
	     (srfi srfi-1) ; for remove
	     (gnu services base) ; for syslog service type
	     (guix store) (gnu services base)
	     (james linux-nonfree)) ; for blobs
(use-service-modules ssh desktop cups admin mcron)
(use-package-modules certs
                     fonts
                     lisp ;; for stumpwm
		     cups ;; for hplip
                     xorg
		     gnome ) ; for Sheila

(define garbage-collector-job
  #~(job "5 0 * * *"
	 "guix gc -F 1G"))

(operating-system
  (host-name "marvin")
  (timezone "US/Eastern")
  (locale "en_US.UTF-8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root"
  ;; is the label of the target root file system.
  (bootloader (grub-configuration (device "/dev/sda")))

  (file-systems (cons (file-system
                        (device "root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (swap-devices '("/dev/sda3"))
  (users (cons* (user-account
		 (name "james")
		 (comment "James Richardson")
		 (group "users")
		 (supplementary-groups '("wheel" "netdev"
					 "audio" "video" "kvm"))
		 (home-directory "/home/james"))
		(user-account
		 (name "sheila")
		 (comment "Sheila Richardson")
		 (group "users")
		 (supplementary-groups '("netdev" "audio" "video"))
		 (home-directory "/home/sheila"))
		%base-user-accounts))
  (kernel linux-nonfree)
  (firmware (cons* iwlwifi-firmware-non-free %base-firmware))
  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (cons* (list sbcl-stumpwm "out") ;stumpwm 
                   nss-certs               ;for HTTPS access
		   font-adobe100dpi
		   font-adobe75dpi
		   font-alias
		   font-dec-misc
		   font-misc-misc
                   font-terminus
		   font-util
		   ;;hplip
                   %base-packages))


  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with Wicd, and more.
  (services (cons* (service openssh-service-type (openssh-configuration))
		   (service cups-service-type
			    (cups-configuration
			     (web-interface? #t)
                             (extensions
                               (list cups-filters hplip))))
		   ;; (service syslog-service-type (syslog-configuration 
		   ;; 				 (config-file (plain-file "syslog.conf" "
		   ;; 							   *.debug /var/log/debug
		   ;; 							   "))))
	           (mcron-service (list garbage-collector-job))
					;(service rottlog-service-type (roittlog-configuration))
		   ;;(gnome-desktop-service)
		   (xfce-desktop-service)
		   (modify-services %desktop-services
				    (guix-service-type config =>
						       (guix-configuration
							(inherit config)
							(substitute-urls (cons* "https://thor.lab01.jamestechnotes.com" %default-substitute-urls))
							(authorize-key? #t)
                                                        (authorized-keys %default-authorized-guix-keys))))))
		   ;; (remove (lambda (service)
		   ;; 	     (or (eq? (service-kind service)
		   ;; 		      syslog-service-type)))
		   ;; 	   %desktop-services)))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
